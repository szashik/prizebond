package com.sz.prizebond;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/*import com.J.jsouptest.MainActivity;
import com.J.jsouptest.R;
import com.J.jsouptest.MainActivity.Result;*/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	String url = "http://www.bangladesh-bank.org/investfacility/prizebond/searchPbond.php?txtNumbers=";
	
	ArrayList<Map> rowData = new ArrayList<Map>();
	EditText edit_input;
	TextView result_text, result_view;
	Button btn_search;	
	ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		edit_input =(EditText) findViewById(R.id.edit_input);
		result_text = (TextView) findViewById(R.id.result_text);
		result_view = (TextView) findViewById(R.id.result_view);
		btn_search = (Button) findViewById(R.id.btn_show);
		
		addButtonListener();
	}
	
	private void addButtonListener() {
		
		btn_search.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				rowData.clear();
				result_text.setText("");
				result_view.setText("");
				new Result().execute();
			}
		});
	}
	
	private class Result extends AsyncTask<Void, Void, Void> {
		String resValue="";
		String wrongResultValue="";
		String cong ="";
 
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(MainActivity.this);
			mProgressDialog.setTitle("Prize Bond Result");
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.show();
		}
 
		@Override
		protected Void doInBackground(Void... params) {
			try {
				// Connect to the web site
				String urlEXT = edit_input.getText().toString();
				String urlU = url + urlEXT;
				Document document = Jsoup.connect(urlU).get();
				
				// get value for missmatch values
				wrongResultValue = document.select("td.asteriskRed").text();
				
				// get values for matched values 
				
				cong = document.select("font.asteriskRed").text();
				
				//storing values in queue  :3
				// finding table
				for(Element table : document.select("table[class=tableData]")){
					// Identify all the row(tr)
					for(Element row : table.select("tr:gt(0)")){
						Map singleResult = new HashMap();
						Elements tds = row.select("td");						
						singleResult.put("Number",tds.get(0).text());						
						singleResult.put("Draw",tds.get(1).text());						
						singleResult.put("Prize",tds.get(2).text());						
						singleResult.put("Amount",tds.get(3).text());						
						rowData.add(singleResult);
					}	
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (wrongResultValue!="") result_text.setText(wrongResultValue); 
			else if(!rowData.isEmpty()){
				String oneValue="";
				result_text.setText(cong);
				for(int i=0;i<rowData.size();i++)
				{
					
					for(Object key: rowData.get(i).keySet())
					{
						oneValue= oneValue+ key.toString() + ": " + rowData.get(i).get(key)+"\n"; 
					}
					oneValue = oneValue + "-----------\n";
				}
				
				result_view.setText(oneValue);
			}
			else result_text.setText("Invalid Input! Please check again.");
			mProgressDialog.dismiss();
		}
	}
	
}
